# Capture the Tanuki!

This repo hosts the run-at-home version of the GitLab CTF which ran from March
16th to March 27th 2020.

# Installation

To run the challenges at home simply clone this repository on your local
machine and run `docker-compose pull` followed by `docker-compose up -d`.
Afterwards the main site should be available at
[`http://capture.local.thetanuki.io`](http://capture.local.thetanuki.io).

Currently eight of a total of twenty challenges are published via this repo.
The missing challenges will be published here within the next weeks.

# Updates

The CTF-at-home platform is still very much work in progress. In order to
update and get the latest released challenges use the script `update.sh` in
this folder.
